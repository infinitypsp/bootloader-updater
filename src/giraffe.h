/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#ifndef GIRAFFE_H_
#define GIRAFFE_H_

#include <psptypes.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int GenerateInit(void *out_prx, u32 *out_size, void *init_enc, u32 init_size, void* systimer_enc, u32 systimer_size, void *carbon_prx, u32 carbon_size);
int GenerateSystimer(void *out_prx, u32 *out_size, void *systimer_enc, u32 systimer_enc_size, void *systimer_dec, u32 systimer_dec_size, void *payload, u32 payload_size);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // GIRAFFE_H_
