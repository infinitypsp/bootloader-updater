/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include <pspsdk.h>
#include <psputils.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "pack_exec.h"
#include "gzip.h"

SceKernelUtilsMt19937Context g_ctx;

int get_random(void)
{
    return sceKernelUtilsMt19937UInt(&g_ctx);
}

void seed_random(void)
{
    sceKernelUtilsMt19937Init(&g_ctx, sceKernelGetSystemTimeLow());
}

int pack_executable(void *out_data, u32 *out_size, void *indata, u32 exec_size, u32 oe_tag, u32 psp_tag, u32 use_gzip, u32 is_custom_key, u32 unused, u8 *ukey, int custom_tag)
{
    PSP_Header psp_header;
    Elf32_Ehdr elf_header_dat;
    //u8 block[16];
    
    memset(&psp_header, 0, sizeof(PSP_Header));
    
    seed_random();
    
    /* set our local variables */
    u32 prx_offset = 0;
    u32 is_kernel_mod = 0;
    
    
    /* Read into the ELF header */
    memcpy(&elf_header_dat, indata, sizeof(elf_header_dat));
    
    /* check if ~PSP already */
    if (elf_header_dat.e_magic == 0x5053507E)
    {
        return -1;
    }
    
    /* ensure it's a PRX and not static elf xD */
    if (elf_header_dat.e_magic != 0x464C457F || elf_header_dat.e_type != 0xFFA0)
    {
        return -1;
    }
    
    /* okay, valid executable, lets load it into memory */
    Elf32_Ehdr *elf_header = (Elf32_Ehdr *)indata;
    
    /* point to the correct program header */
    int phnum;
    u32 moduleinfo_offset = 0;
    Elf32_Phdr *program_header = (Elf32_Phdr *)((u32)elf_header + elf_header->e_phoff);
    
    /* loop through the sections */
    for (phnum = elf_header->e_phnum; phnum > 0; phnum--, program_header++)
    {
        /* check the p_type */
        if (program_header->p_type == 1)
        {
            /* check if it's a kernel module */
            is_kernel_mod = (program_header->p_paddr >> 31);
            
            /* check for moduleinfo offset */
            if (program_header->p_vaddr == program_header->p_paddr)
            {
            }
            else
            {
                /* this is a moduleinfo offset! lets set it */
                psp_header.modinfo_offset = program_header->p_paddr;
                moduleinfo_offset = (program_header->p_paddr & 0x7FFFFFFF);
            }
            
            break;
        }
    }
    
    /* seek to the moduleinfo offset */
    sceModInfo *modinfo = (sceModInfo *)((u32)elf_header + moduleinfo_offset);
    
    /* check the kmode comparisions */
    if ((is_kernel_mod && (modinfo->modattribute & 0x1000) == 0) || (!is_kernel_mod && (modinfo->modattribute & 0x1000)))
    {
        /* mix privilages */
        //printf("This executable has mixed kernel privilages!\n");
        return -1;
    }
    
    /* check for PBP and gzip */
    if (is_kernel_mod)
    {
        /*check if no oe_tag */
        if ((oe_tag == 0 && psp_tag == 0) || (is_kernel_mod))
        {
            if (!custom_tag)
            {
                /* set to M33 tags */
                oe_tag = 0x55668D96;
                psp_tag = 0xDADADAF0;
            }
        }
    }
    
    /* check for usermode M33 mod */
    if (oe_tag == 0 || psp_tag == 0)
    {
        oe_tag = 0x8555ABF2;
        psp_tag = 0x457B06F0;
    }
    
    /* fill the psp_header */
    psp_header.signature = 0x5053507E;
    psp_header.attribute = modinfo->modattribute;
    
    /* if not gzip, use RLZ ;) */
    psp_header.comp_attribute = (use_gzip) ? (0x1) : (0x401);
    psp_header.module_ver_lo = modinfo->modversion[0];
    psp_header.module_ver_hi = modinfo->modversion[1];
    strcpy(psp_header.modname, modinfo->modname);
    psp_header._80 = 0x80;
    
    /* preset version to 1 */
    psp_header.version = 1;
    
    /* set the elf size and entry */
    psp_header.elf_size = exec_size;
    psp_header.entry = elf_header->e_entry;
    
    /* get the number of segments */
    psp_header.nsegments = (elf_header->e_phnum > 2) ? (2) : (elf_header->e_phnum);
    
    /* check for no segments */
    if (psp_header.nsegments == 0)
    {
        //printf("There are no segments!\n");
        return -1;
    }
    
    /* get the program header */
    program_header = (Elf32_Phdr *)((u32)elf_header + elf_header->e_phoff);
    
    int i;
    /* loop through the number of segments */
    for (i = 0; i < psp_header.nsegments; i++)
    {
        /* copy the segment info */
        psp_header.seg_align[i] = program_header[i].p_align;
        psp_header.seg_address[i] = program_header[i].p_vaddr;
        psp_header.seg_size[i] = program_header[i].p_memsz;
    }
    
    Elf32_Shdr *sections = (Elf32_Shdr *)((u32)elf_header + elf_header->e_shoff);
    char *strtab = (char *)(sections[elf_header->e_shstrndx].sh_offset + (u32)elf_header);
    
    /* go through the sections looking for .bss section */
    for (i = 0; i < elf_header->e_shnum; i++)
    {
        /* check if this section is called ".bss" */
        if (strcmp(strtab + sections[i].sh_name, ".bss") == 0)
        {
            /* copy over the .bss size */
            psp_header.bss_size = sections[i].sh_size;
            break;
        }
    }
    
    /* check if we didn't find .bss */
    if (i == elf_header->e_shnum)
    {
        /* error D: */
        //printf("Error, could not find .bss section!\n");
        return 0;
    }
    
    /* check for kernel param */
    if (psp_header.attribute & 0x1000)
    {
        /* check for boot attribute */
        if (psp_header.attribute & 0x2000)
        {
            /* set devkit to 6.31 */
            psp_header.devkitversion = 0x06030110;
        }
        else
        {
            /* set devkit to 5.71 */
            psp_header.devkitversion = 0x05070110;
        }
        
        /* set the decrypt mode */
        psp_header.decrypt_mode = 2;
    }
    else
    {
        /* is user mode, check for PBP */
        if (prx_offset)
        {
            /* check for VSH API (updater) */
            if (psp_header.attribute & 0x800)
            {
                /* set the decryption mode to updater */
                psp_header.decrypt_mode = 0xC;
            }
            
            /* check for APP API (comics, etc) */
            else if (psp_header.attribute & 0x600)
            {
                /* set the decryption mode to app */
                psp_header.decrypt_mode = 0xE;
            }
            
            /* check for USB WLAN API (skype, etc) */
            else if (psp_header.attribute & 0x400)
            {
                /* set the decryption mode to USB WLAN */
                psp_header.decrypt_mode = 0xA;
            }
            
            /* else set to MS API */
            else
            {
                /* could check the SFO for POPS... */
                psp_header.attribute |= 0x200;
                modinfo->modattribute |= 0x200;
                psp_header.decrypt_mode = 0xD;
            }
            
            /* set devkit to 5.72 */
            psp_header.devkitversion = 0x05070210;
        }
        else
        {
            /* is standalone PRX */
            
            /* check for VSH API */
            if (psp_header.attribute & 0x800)
            {
                /* set the decrypt mode */
                psp_header.decrypt_mode = 0x3;
            }
            
            else
            {
                /* set to standard */
                psp_header.devkitversion = 0x05070210;
                psp_header.decrypt_mode = 4;
            }
        }
    }
    
    /* copy over the tag */
    psp_header.oe_tag = oe_tag;
    psp_header.tag = psp_tag;
    
    /* get the predicted max compress size */
    //u32 predict_size = gzipGetMaxCompressedSize(exec_size);
    
    /* compress using gzip */
    if (use_gzip)
    {
        /* compress!! */
        int res = gzipCompress(out_data + sizeof(PSP_Header), exec_size, elf_header, exec_size);
        
        /* check for gzip compression error */
        if (res < 0)
        {
            /* error */
            return res;
        }
        
        /* set the psp_header value */
        psp_header.comp_size = res;
    }
    
    /* set PSP size */
    psp_header.psp_size = psp_header.comp_size + sizeof(PSP_Header);
    
    /* fill a section with random data */
    for (i = 0; i < 0x1C/4; i++)
    {
        ((u32 *)psp_header.key_data3)[i] = get_random();
    }
    
    {
        /* randomise data */
        for (i = 0; i < 0x30/4; i++)
        {
            ((u32 *)psp_header.key_data0)[i] = get_random();
        }
        
        for (i = 0; i < 0x10/4; i++)
        {
            ((u32 *)psp_header.key_data1)[i] = get_random();
        }
        
        psp_header.key_data2 = get_random();
    }
    
    /* copy psp_header to buffer */
    memcpy(out_data, &psp_header, sizeof(PSP_Header));
    
    *out_size = psp_header.psp_size;

    
    /* return success */
    return 0;
}
