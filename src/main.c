#include "giraffe.h"

#include <pspdecrypt.h>
#include <infinity_kinstaller.h>

#include <pspsdk.h>
#include <pspiofilemgr.h>

#include <string.h>
#include <malloc.h>

PSP_MODULE_INFO("bootloader-updater", 0, 1, 0);

const char *g_last_error = NULL;

static int WriteFile(char *file, void *buf, int size)
{
    SceUID fd = sceIoOpen(file, PSP_O_WRONLY | PSP_O_CREAT | PSP_O_TRUNC, 0777);
    
    if (fd < 0)
    {
        return fd;
    }

    int written = sceIoWrite(fd, buf, size);

    sceIoClose(fd);
    return written;
}

static void setError(const char *error)
{
    g_last_error = error;
}

const char *infBootloaderUpdaterLastError(void)
{
    return g_last_error;
}

static int decrypt_decompress_prx(u8 *systimer, unsigned int size, u8 *workbuffer, unsigned int worksize)
{
    int res = pspDecryptPRX(systimer, workbuffer, size);
    
    if (res < 0)
    {
        return -1;
    }

    u8 *dec_prx = workbuffer + res;
    while ((u32)dec_prx % 64) dec_prx++;

    int dec_prx_size = pspDecompress(workbuffer, dec_prx, (u32)workbuffer+worksize-(u32)dec_prx);

    if (dec_prx_size < 0)
    {
        return -2;
    }
    
    memcpy(workbuffer, dec_prx, dec_prx_size);
    return dec_prx_size;
}

int infBootloaderUpdaterFlashBootloader(u8 *workbuffer, unsigned int worksize, u8 *systimer, unsigned int systimer_size, u8 *init, unsigned int init_size)
{
    char file[256];
    int size, cur, total;
    u8 *bootloader = NULL, *payload = NULL;
    int bootloader_size = 0, payload_size = 0;
    
    if (mfcOpenSection((char *)"BOOTLOADER") < 0)
    {
        setError("Error opening section \"BOOTLOADER\".");
        return -1;
    }
    
    while (mfcGetNextFile(file, &size, &cur, &total, workbuffer, worksize) >= 0)
    {
        if (strcmp(file, "bootloader.prx") == 0)
        {
            bootloader = malloc(size);
            
            if (bootloader == NULL)
            {
                setError("Error allocating bootloader memory.");
                mfcCloseSection();
                return -2;
            }
            
            memcpy(bootloader, workbuffer, worksize);
            bootloader_size = size;
        }
        
        else if (strcmp(file, "bootloader_payload.bin") == 0)
        {
            payload = malloc(size);
            
            if (payload == NULL)
            {
                setError("Error allocating bootloader payload memory.");
                mfcCloseSection();
                return -3;
            }
            
            memcpy(payload, workbuffer, worksize);
            payload_size = size;
        }
    }
    
    if (mfcCloseSection() < 0)
    {
        setError("Could not close MFC section.");
        return -9;
    }
    
    // decrypt systimer
    int dec_systimer_size = decrypt_decompress_prx(systimer, systimer_size, workbuffer, worksize);
    
    if (dec_systimer_size < 0)
    {
        setError("Could not decrypt 6.31 systimer.prx");
        return -4;
    }
    
    if (pspSignCheck(systimer) < 0)
    {
        setError("could not signcheck systimer.prx");
        return -5;
    }
    
    u8 *giraffe_systimer = workbuffer+dec_systimer_size;
    while ((u32)giraffe_systimer % 64) giraffe_systimer++;
    unsigned int giraffe_systimer_size = 0;
    
    if (GenerateSystimer(giraffe_systimer, &giraffe_systimer_size, systimer, systimer_size, workbuffer, dec_systimer_size, payload, payload_size) < 0)
    {
        setError("Could not generate/sign systimer.prx");
        return -6;
    }
    
    memcpy(workbuffer, giraffe_systimer, giraffe_systimer_size);
    giraffe_systimer = workbuffer;
    
    // build our init.prx replacement
    if (pspSignCheck(init) < 0)
    {
        setError("Could not signcheck init.prx");
        return -7;
    }
    
    u8 *packed_bootloader = giraffe_systimer+giraffe_systimer_size;
    while ((u32)packed_bootloader % 64) packed_bootloader++;
    unsigned int packed_bootloader_size = 0;

    if (GenerateInit(packed_bootloader, &packed_bootloader_size, init, init_size, systimer, systimer_size, bootloader, bootloader_size) < 0)
    {
        setError("Could not create init.prx replacement.");
        return -8;
    }
    
    if (WriteFile("flash0:/kd/systimer.prx", giraffe_systimer, giraffe_systimer_size) != giraffe_systimer_size)
    {
        // no point exiting, it'll be a brick regardless
        setError("Error writing bootloader systimer.prx.");
    }
    
    if (WriteFile("flash0:/kd/init.prx", packed_bootloader, packed_bootloader_size) != packed_bootloader_size)
    {
        // no point exiting, it'll be a brick regardless
        setError("Error writing bootloader init.prx.");
    }

    setError(NULL);
    return 0;
}

int module_start(SceSize args, void *argp)
{
    return 0;
}
