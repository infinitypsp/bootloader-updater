TARGET = bootloader_updater
OBJS = src/main.o src/giraffe.o src/pack_exec.o src/gzip.o \
        libasm/pspDecrypt.o \
        libasm/infinity_kinstaller.o

INCDIR = include .
CFLAGS = -std=c99 -O2 -G0 -Wall
CXXFLAGS = $(CFLAGS) -fno-builtin -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS) -c

PRX_EXPORTS = src/exports.exp

LIBDIR = lib
LDFLAGS = -nostdlib -nodefaultlibs
LIBS = -lz

BUILD_PRX = 1
USE_KERNEL_LIBS = 0
USE_PSPSDK_LIBC = 1

PSPSDK=$(shell psp-config --pspsdk-path)
include $(PSPSDK)/lib/build.mak
